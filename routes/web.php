<?php


Route::get('/', function () {
    return view('welcome');
});


Route::get('/profile', function () {
    return ('Ini Adalah Page Profile');
});

// Route::get('/profile_view', function () {
//     return view('page_profile');
// });


Route::get('/contact', function () {
    return view('templates.contact');
});

Route::get('/content/{id}','ContohController@tampilkan');

Route::get('/input_form','ContohController@input');

Route::post('/input_form_post','ContohController@input_post');


Route::get('/table','ContohController@table');

